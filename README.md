# Welcome to the  ZeroTwoBot Rep

Hi, I'm Aron a Student from Germany and the main programmer for this Discord-Bot. This is opensource project that hopefully turns into a well-featured option for people who want that tad bit more control over their server than Discord gives them.

##### The main-reason for creating this bot was better management of our Hentai-Discord-Server [Hentai-Revive](https://discord.gg/UAUPG9W)


## Features
- Extensive channel tracking utilizing a sqlite database which enables you to enforce more specific restrictions and options for each and every channel
- Easily extandable command-system that yet again utilizies a sqlite db. Allows for bot global aswell as server-specific commands. (If command exists both global and server-specific, the global command on those servers will be overwritten)
- Built in option for media-only channels, where the bot will delete any messages that do not include images or a link. Can be easily modified to include videos and gifs
- Bot changes it's profile picture every 11 minutes (to be on the save side with the 10min cooldown) to a random picture from the media folder.
- Bot changes status every minute to a random one from `util/stati.txt`
- Bot currently uses the [`hmtai`](https://www.npmjs.com/package/hmtai) library but will soon transition to our in-house [`a-hentai`](https://www.npmjs.com/package/a-hentai) library.

## The public Bot

We host a public instance of the bot that runs on the most current state of the master branch, which you are free to invite to your server anytime, if you can't be bothered forking the bot and hosting it yourself.
As of now, the stability of the bot is kinda meh, but hopefully it will increase drastically in the future.

#### [Invite the Bot](https://discord.com/oauth2/authorize?client_id=832473300062961674&permissions=537159744&scope=applications.commands%20bot)
> If you invite the Bot, please make sure it has full permissions, otherwise it cannot work properly

### Using the Bot

#### The default Prefix is `02!`
- You can use `02!ping` to test wether the bot is online
- Use `02!prefix {prefix}` to change the prefix
- If you want to track a channel use `02!tack-channel {restriction}`
	- (Only 1 restriction can be added at a time)
		- Availabel Restrictions:
		- `mediaonly` to only allow media messages
		- `hentai={tag}` to have the bot post images from the hmtai library to this channel (Please refer to the library to see which tags are available)
		- `onlineonly` to allow only users that are not shown as offline to chat
-	To remove all restrictions use `02!untrack-channel`

## Forking the Bot

This bot is opensource and you're free to fork it and improve or extend it any way, shape or form you'd like. The easiest and best way to do so, is to either create an issue telling us which features you want to see, or by cloning the git-rep to your pc and creating merge-request enabling other people to utilize your newly created features.

### Adding Commands
- Adding Commands is easy
- Create a new {command}.js file in `command-loader/commands/` 
	- (You can use the ping command as a template)
- Add the command to the database utilizing the given command in `index.js`
	- (Make sure you set the right parameters)
- And you're done. You can now use the command in your bot, you don't even have to restart it.