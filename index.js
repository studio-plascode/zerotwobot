const Discord = require('discord.js');
const fs = require('fs');

const commandLoader = require('./command-loader/command-loader');

const client = new Discord.Client();

client.commands = new Discord.Collection();

const util = {};

const utilFiles = fs.readdirSync("./util/").filter(file => file.endsWith(".js"));

for (const file of utilFiles) {
    const ut = require(`./util/${file}`);

    util[ut.name] = ut;
}

//channel tracker
const CT = util["channelTracking"];
//settings manager
const settings = util["settings"];

//send message to console once bot is online
client.once("ready", () => {
    console.log("Bot online as: " + client.user.username);

    //initializing Command system
    commandLoader.init(client, logToFile, util, commandLoader);

    //INSERT NEW COMMANDS MANUALLY HERE
    //commandLoader.insertCommand({COMMAND-NAME}, {SCOPE(undefined for cross-server usage)}, {ARGUMENTS(sperated by commas, no spaces)});
    //REMOVE COMMANDS MANUALLY HERE
    //commandLoader.removeCommand({COMMAND-NAME}, {SCOPE});

    //refresh PfP Timer
    setInterval(util["refreshPfP"].execute, 660000, client, logToFile);
    //refresh Status
    setInterval(util["refreshStatus"].execute, 60000, client, logToFile);
});

//do stuff once bot is invited
client.on("createGuild", guild => {
    //init server settings
    settings.newServer(guild.id, logToFile);
});

client.on("message", message => {
    try {
        settings.getPrefix(message.guild.id, logToFile).then((prefix)=>{

            if (!message.author.bot && !message.content.startsWith(prefix)) {
                CT.getRestriction(message.channel.id, logToFile).then((res) => {
                    if(res==undefined){}
                    else{
                        if (res.includes("mediaonly")) {
                            util["mediaOnlyChannels"].execute(message, logToFile);
                        }
                        if (res.includes("onlineonly")) {
                            util["onlineOnlyChannels"].execute(message, logToFile);
                        }
                    }
                });//CT.getRestriction
            }

            //check if message starts with prefix, or is send by bot itself.
            if (!message.content.startsWith(prefix) || message.author.bot) return;

            const args = message.content.slice(prefix.length).split(/ +/);
            const command = args.shift().toLowerCase();

            commandLoader.executeLocalCommand(command, message.guild.id, message, args);
        });
    } catch (e) {
        console.log("Bot crashed on Message: " + message.content);
        logToFile("Bot crashed on Message: " + message.content);
        console.log("Error:   " + e);
        logToFile("Error:   " + e);
    }
});

const logger = fs.createWriteStream('log.txt', {
    flags: 'a'
});

function logToFile(logContent) {
    let time = new Date().toString();
    setTimeout(() => {
        logger.write(time + "   -   " + logContent + "\n")
    }, 100);
}

function checkIfInArr(value, array) {
    let result = false;
    for (let item in array) {
        if (value == item)
            result = true;
    }
    return result;
}

process.on('uncaughtException', (err) => {
    logToFile('There was an uncaught error:  ', err);
    console.error('There was an uncaught error:  ', err);
    process.exit(1); //mandatory (as per the Node.js docs)
});

//bring bot online - line must be last line
client.login(process.argv.slice(2)[0]);