const Discord = require('discord.js');
const sqlite3 = require('sqlite3');

let db = new sqlite3.Database('command-loader/commands.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to "commands.db" SQLite database.');
  });

const publicArgs = {
    discordJS: require("discord.js"),
    logToFile: undefined,
    client: undefined,
    util: undefined
};

let client;

module.exports = {
    name: "command-loader",
    description: "Loads commands from DB",

    init(initClient, initLogToFile, initUtil){
        publicArgs.client = initClient;
        publicArgs.logToFile = initLogToFile;
        publicArgs.util = initUtil;
        client = initClient;
        client.commands = new Discord.Collection();
    },

    getLocalCommand(command, guildID){
        return new Promise((resolve, reject)=>{
            let localCommand;
            db.all(`SELECT id, command, args, path FROM commands WHERE command = ? AND (scope = "GLOBAL" OR scope = ?)`, [command, guildID] , (err, rows)=>{
                if(err){
                    console.log(err);
                    logToFile(err);
                    reject(err);
                }

                if(rows.length==0)
                    reject("No such command!");
                else {
                    localCommand = rows[0];
                    for(let i = 0; i < rows.length; i++){
                        if(rows[i].scope != "GLOBAL")
                            localCommand = rows[i];
                    }

                    if(!client.commands.has(localCommand.id))
                        client.commands.set(localCommand.id, require(localCommand.path));

                    resolve(localCommand);
                }
            });
        });
    },

    prepareArguments(args, localArgs){
        args = args.trim().split(",");
        let arguments = [];

        for(let i = 0; i < args.length; i++){
            const arg = args[i];
            if(arg in publicArgs)
                arguments.push(publicArgs[arg]);
            else if(arg in localArgs)
                arguments.push(localArgs[arg]);
            else if(arg in publicArgs["util"])
                arguments.push(publicArgs["util"][arg]);
        }

        return arguments;
    },

    executeLocalCommand(command, guildID, message, mArgs){
        this.getLocalCommand(command, guildID).then((res)=>{
            client.commands.get(res.id).execute(this.prepareArguments(res.args, {message: message, mArgs: mArgs}));
        }).catch((e)=>{
            console.log(e);
            logToFile(e);
        });
    },

    insertCommand(command, scope, args){
        const path = "./commands/"+command;
        if(scope==undefined)
            scope = "GLOBAL";
        const id = scope+command;
        db.run(`INSERT INTO commands Values(?,?,?,?,?)`, [id, command, scope, args, path] , (err)=>{
            if(err){
                console.log(err);
                logToFile(err);
            };
        });
    },

    removeCommand(command, scope){
        db.run(`DELETE FROM commands WHERE id = ?`, [scope+command] , (err)=>{
            if(err){
                console.log(err);
                logToFile(err);
            };
        });
    }
}