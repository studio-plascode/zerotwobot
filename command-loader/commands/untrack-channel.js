module.exports = {
    name: "untrack-channel",
    description: "removes channel from tracking",

    //args = message, channelTracking, logToFile
    execute(args){

        const message = args[0];
        const untrackChannel = args[1].untrackChannel;
        const logToFile = args[2];

        if(!message.member.hasPermission("ADMINISTRATOR")){
            message.react("❌");
                message.channel.send("You need to be an Admin!").then(msg => {
                    setTimeout(()=>{
                        message.delete();
                        msg.delete();
                    },3000);
                });
            return;
        }

        try{
            untrackChannel(message.channel.id, logToFile);
            console.log(message.channel + " is no longer being tracked");
            logToFile(message.channel + "is no longer being tracked");
            message.react("✅");
            message.channel.send(message.guild.channels.cache.get(message.channel.id).toString() + " is no longer being tracked").then(msg => {
                setTimeout(()=>{
                    message.delete();
                    msg.delete();
                },10000);
            });
        }
        catch(e){
            console.log(e);
            logToFile(e);
            message.react("❌");
            message.channel.send("An Error occured").then(msg => {
                setTimeout(()=>{
                    message.delete();
                    msg.delete();
                },3000);
            });
        }
    }
}