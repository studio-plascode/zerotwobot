module.exports = {
    name: "prefix",
    description: "sets new prefix for server",

    //args = message, settings, args, logToFile
    execute(args){
        const message = args[0];
        const settings = args[1];
        const argument = args[2][0];
        const logToFile = args[3];

        if (message.member.hasPermission("ADMINISTRATOR")) {
            settings.setSetting(message.guild.id, "prefix", argument, logToFile);
            console.log("Prefix changend to: " + argument);
            logToFile("Prefix changend to: " + argument);
            message.react("✅");
            message.channel.send("Prefix changend to: `" + argument + "`.").then(msg => {
                setTimeout(() => {
                    message.delete();
                    msg.delete();
                }, 10000);
            });
        }
    }
}