module.exports = {
    name: "ping",
    description: "Returns Pong",

    //args = message
    execute(args){
        const message = args[0];

        console.log("COMMAND: ping")
        message.channel.send("Pong");
    }
}