const { jahy } = require("hmtai");

module.exports = {
    name: "hentai",
    description: "Sends Hentai to the current channel",

    //args = message, logToFile, channelTracking
    execute(args){
        const message = args[0];
        const logToFile = args[1];
        const CT = args[2];

        const hmtai = require("hmtai");
        const h = hmtai.nsfw;
        var picture;

        try {
            CT.getRestriction(message.channel.id, logToFile).then((res) => {
				if(!res.includes("hentai")){
					message.react("❌");
					message.channel.send("Channel not supported").then(msg => {
						logToFile("Hentai command requested in invalid channel("+message.channel+") by  "+message.author);
						setTimeout(()=>{
							message.delete();
							msg.delete();
						},3000);
					});
					return;
				}
			});

            this.getFunction(message.channel.id, logToFile, CT).then((res) => {
                message.channel.send(eval(res));
                message.delete();
            });
        }

        catch(e){
            console.log(e);
            logToFile(e);
        }
    },

    getFunction(channelID, logToFile, CT){
        return new Promise((resolve, reject)=>{
            CT.getRestriction(channelID, logToFile).then((res)=>{
                if(!res.includes("hentai"))
                    reject("Channel not tracked");
                res = res.split(",");
                var tag;
                for(var i = 0; i < res.length; i++){
                    if(res[i].includes("hentai="))
                        tag = res[i].split("=")[1];
                }
                resolve("(h."+tag+"())");
            });
        });
    }
}