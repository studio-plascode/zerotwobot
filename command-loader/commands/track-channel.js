module.exports = {
    name: "track-channel",
    description: "adds channel to tracking",

    //args = message, mArgs, channelTracking, logToFile
    execute(args){

        const message = args[0];
        const arg = args[1][0];
        const trackChannel = args[2].trackChannel;
        const logToFile = args[3];

        if(!message.member.hasPermission("ADMINISTRATOR")){
            message.react("❌");
                message.channel.send("You need to be an Admin!").then(msg => {
                    setTimeout(()=>{
                        message.delete();
                        msg.delete();
                    },3000);
                });
            return;
        }

        try{
            if(arg!="mediaonly"&&!arg.startsWith("hentai=")&&arg!="onlineonly"){
                message.react("❌");
                message.channel.send("Invalid Restriction Type.").then(msg => {
                    setTimeout(()=>{
                        message.delete();
                        msg.delete();
                    },3000);
                });
            }
            else{
                trackChannel(message.channel.id, message.guild.id, arg, logToFile);
                console.log(message.channel + " is being tracked now under the  " + arg + "  restriction.");
                logToFile(message.channel + " is being tracked now under the  " + arg + "  restriction.");
                message.react("✅");
                    message.channel.send(message.guild.channels.cache.get(message.channel.id).toString() + " is being now tracked under the `" + arg + "` restriction.").then(msg => {
                        setTimeout(()=>{
                            message.delete();
                            msg.delete();
                        },10000);
                    });
                }
            }
        catch(e){
            console.log(e);
            logToFile(e);
            message.react("❌");
            message.channel.send("An Error occured").then(msg => {
                setTimeout(()=>{
                    message.delete();
                    msg.delete();
                },3000);
            });
        }
    }
}