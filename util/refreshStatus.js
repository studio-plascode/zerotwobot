module.exports = {
    name: "refreshStatus",
    description: "refreshes Bots Status",
    execute(client, logToFile){
        var index = 0;
        var stati = [];

        require('fs').readFileSync('util/stati.txt', 'utf-8').split(/\r?\n/).forEach(function(line){
            stati[index] = line;
            index++;
        });

        const indexPicked = Math.floor(Math.random() * stati.length);
        const statusPicked = stati[indexPicked].split("§");
        client.user.setActivity(statusPicked[1], { type: statusPicked[0]});

        console.log("Status set: "+statusPicked);
        logToFile("Status set: "+statusPicked);
    }
}