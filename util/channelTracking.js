const sqlite3 = require('sqlite3');

let db = new sqlite3.Database('util/channels.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to "channels.db" SQLite database.');
});

module.exports = {
    name: "channelTracking",
    description: "Keeps track of channels",

    untrackChannel(channelID, logToFile) {
        db.run(`DELETE FROM channels WHERE channelid = ?`, [channelID], (err) => {
            if (err) {
                console.log(err);
                logToFile(err);
            }
        });
    },

    getTrackedGuildChannels(guildID, logToFile) {
        return new Promise((resolve, reject)=>{
            let trackedChannels = [];
            db.all(`SELECT channelid FROM channels WHERE guildid = ?`, [guildID], (err, rows) => {
                if (err) {
                    console.log(err);
                    logToFile(err);
                    reject(err);
                }
                for (let i = 0; i < rows.length; i++) {
                    trackedChannels[i] = rows[i].channelid;
                }
                console.log(trackedChannels);
                resolve(trackedChannels);
            });
        });
    },

    getRestriction(channelID, logToFile) {
        try {
            return new Promise((resolve, reject) => {
                db.get(`SELECT restriction FROM channels WHERE channelid = ?`, [channelID], (err, row) => {
                    if (err) {
                        console.log(err);
                        logToFile(err);
                        reject(err);
                    }
                    if (row !== undefined) {
                        console.log(row.restriction);
                        resolve(row.restriction);
                    } else
                        resolve(undefined);
                });
            });
        } catch (e) {
            console.log(e);
        }
    },

    trackChannel(channelID, guildID, restriction, logToFile) {
        module.exports.getRestriction(channelID, logToFile).then((res) => {
            if (res != undefined && res != restriction) {
                db.run(`UPDATE channels SET restriction = ? WHERE channelid = ?`, [res + "," + restriction, channelID], (err) => {
                    if (err) {
                        console.log(err);
                        logToFile(err);
                    }
                });
            } else {
                db.run(`INSERT INTO channels Values(?,?,?)`, [channelID, guildID, restriction], (err) => {
                    if (err) {
                        console.log(err);
                        logToFile(err);
                    }
                });
            }
        });
    },
}