module.exports = {
    name: "onlineOnlyChannels",
    description: "Removes Messages from users shown as offline",

    execute(message, logToFile){
        if(message.author.presence.status!="offline")
            return;
        
        logToFile(message.author+" chatted in the wrong channel.("+message.channel+")");
        message.author.send("You have to be online to chat in this channel.");
        message.delete();
    }
}