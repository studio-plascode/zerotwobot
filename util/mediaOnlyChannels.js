module.exports = {
    name: "mediaOnlyChannels",
    description: "Removes non Media Messages",

    execute(message, logToFile){
        if(message.content.includes("http"))
            return;
        if(!message.attachments.every((file)=>{
            console.log(file);
            if(file.substr(file.length-3)=="png"||file.substr(file.length-3)=="jpg"||file.substr(file.length-4)=="jpeg"||file.substr(file.length-3)=="gif")
                return true;
            else
                return false;
        }))
            return;
        
        logToFile(message.author+" chatted in the wrong channel.("+message.channel+")");
        message.author.send("Please don't chat in a media-only channel.");
        message.delete();
    }
}