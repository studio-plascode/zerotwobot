const sqlite3 = require('sqlite3');

let db = new sqlite3.Database('util/settings.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to "settings.db" SQLite database.');
});

module.exports = {
    name: "settings",
    description: "Keeps server settings",

    newServer(guildID, logToFile){
        db.run(`INSERT INTO settings Values(?,?)`, [guildID, "prefix=02!"], (err) => {
            if (err) {
                console.log(err);
                logToFile(err);
            }
        });
    },

    getSettings(guildID, logToFile){
        return new Promise((resolve, reject)=>{
            db.get(`SELECT settings FROM settings WHERE guildid = ?`, [guildID], (err, row) => {
                if (err) {
                    console.log(err);
                    logToFile(err);
                    reject(err);
                }
                if(row==undefined)
                    reject("NO SETTINGS");

                resolve(row.settings);
            });
        });
    },

    getPrefix(guildID, logToFile){
        return new Promise((resolve, reject)=>{
            db.get(`SELECT settings FROM settings WHERE guildid = ?`, [guildID], (err, row) => {
                if (err) {
                    console.log(err);
                    logToFile(err);
                    reject(err);
                }
                if(row==undefined){
                    reject("Error");
                    logToFile("Lookup for settings failed");
                    console.log("Lookup for settings failed");
                }
                const settings = row.settings.split(",");
                for(var i = 0; i < settings.length; i++){
                    if(settings[i].includes("prefix="))
                        resolve(settings[i].split("=")[1]);
                }
            });
        });
    },

    setSetting(guildID, settingToSet, newValue, logToFile){
        this.getSettings(guildID, logToFile).then((res)=>{
            const settings = res.split(",");
            var newSettings = "";
            for(var i = 0; i < settings.length; i++){
                if(settings[i]==undefined){}
                else if(settings[i].includes(settingToSet+"="))
                    newSettings+= settingToSet+"="+newValue;
                else
                    newSettings+=settings[i]+",";
            }

            db.run(`UPDATE settings SET settings = ? WHERE guildid = ?`, [newSettings, guildID], (err) => {
                if (err) {
                    console.log(err);
                    logToFile(err);
                }
            });
        });
    },

    setSettings(guildID, newSettings, logToFile){
        db.run(`UPDATE settings SET settings = ? WHERE guildid = ?`, [newSettings, guildID], (err) => {
            if (err) {
                console.log(err);
                logToFile(err);
            }
        });
    },
}