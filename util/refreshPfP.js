module.exports = {
    name: "refreshPfP",
    description: "refreshes Bots PfP",
    execute(client, logToFile){
        const fs = require('fs');
        const pictures = [];
        const pictureFiles = fs.readdirSync("media/");
        var picI = 0;

        for (const file of pictureFiles) {
            picI++;
            const picture = `media/${file}`;

            pictures[picI] = picture;
        }

        const indexPicked = Math.floor(Math.random() * pictures.length-1);

        //check if bot picked actual picture
        if(pictures[indexPicked]==undefined)
            module.exports.execute(client, logToFile);
        else
            client.user.setAvatar(pictures[indexPicked]);

        console.log("refreshed pfp to: "+pictures[indexPicked]);
        logToFile("refreshed pfp to: "+pictures[indexPicked]);
    }
}
